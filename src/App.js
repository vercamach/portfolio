import React from 'react';
import AboutMe from "./components/about/AboutMe"
import Projects from "./components/projects/Projects"
import Contact from "./components/contact/Contact"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import './App.css';
// import './starsky.css';
// import Logo from './logo';


export default function App() {
  const projects = [
    {
      src: "./img/weather.png",
      stack: "ReactJS, CSS",
      text: "School project using third party API done in ReactJS. I created the design, fetched the API, divided it to future weatger information and current weather information. There is weather for two specific cities and one page where the user can submit a form and get weather for a city of his choice.",
      link: "http://veru.dk/weatherApp"
    },
    {
      src: "./img/dogstagram.png",
      stack: "HTML, PHP, CSS, MySQL",
      text: "Main purpose of this project is to showcase my ability to work with a relational database. I made database diagram with full normalization, created the design and implemented full CRUD. User can sign up, verify email, log in, upload, alter and delete pictures, rate pictures, change information, upload, alter and delete profile picture and in admin section user with admin privileges can delete any images abd block users.",
      text2: "login: admin password: password, or you can sign up ",
      link: "http://veru.dk/goodboy/login"

    },
    {
      src: "./img/bank.png",
      stack: "HTML, PHP, CSS, Text-file with data in JSON",
      text: "School project done in HTML, PHP, CSS and text file used as a database. Fully functional login, signup and password change with authentication, picture uploading and simulated admin page.",
      text2: "login: 12341234 password: password, or you can sign up ",
      link: "http://veru.dk/bank/login"
    },
    {
      src: "./img/bf.png",
      stack: "GatsbyJS, CSS",
      text: "Website for company Billetfix was orriginally done in plain HTML, CSS and JavaScript and my task as an intern was to re-do the website in ReactJS using GatsbyJS.",
      link: "https://billetfix.dk/da/"

    },
    {
      src: "./img/tb.png",
      stack: "ReactJs, GatsbyJS, CSS",
      text: "Marketing  homepage for copmany Ticketbutler. Currently I am manintaining and updating the website",
      link: "https://ticketbutler.io/da/"
    },
    {
      src: "./img/tb-dashboard.png",
      stack: "ReactJs, CSS",
      text: "Ticketbutler selfonboarding enabling customers to create their own whitelabel, choosing colors, font, and logo as well as registering their company and user.",
      link: "https://you.ticketbutler.io/en/create/"
    },
  ]

  return (
    <Router>
      <div class="starysky_blue"
        style={{
          backgroundColor: "rgb(138, 117, 136)", height: "100vh",
          backgroundAttachment: "fixed"
        }}
      >
        <div id="stars" />
        <div id="stars2" />
        <div id="stars3" />

        <nav>
          <Link to="/">About me</Link>
          <Link to="/projects">Projects</Link>
          <Link to="/contact">Contact</Link>
        </nav>

        <div class="container">
          <Switch>
            <Route path="/projects">
              <div class="column-divider">
                {projects.map(project =>
                  <Projects src={project.src} text={project.text} text2={project.text2} s stack={project.stack} link={project.link} />
                )}
              </div>
            </Route>
            <Route path="/contact">
              <Contact />
            </Route>
            <Route path="/">
              <AboutMe />
            </Route>
          </Switch>
        </div>
      </div>
    </Router >
  );
}