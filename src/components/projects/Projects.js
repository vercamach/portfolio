import React from "react";
import "./projects.css"
import { Modal } from "../modal"

export default function Projects({ src, text, stack, text2, link }) {
    const [open, setOpen] = React.useState(false);

    console.log(src)
    return (
        <div>
            <div className="post">
                <div
                    className="backgroundImage"
                    style={{ backgroundImage: "" }}
                ></div>

                <div className="image">
                    <div id="imageEmotionDisplay">
                        <img className="images" src={require(`${src}`)} alt="image" />
                    </div>
                </div>

                <div className="imageHover">
                    <button
                        className="moreInfoButton"
                        onClick={() => {
                            setOpen(true);
                        }}
                    >
                        Show more
        </button>
                </div>
            </div>
            <Modal
                isOpen={open}
                onClose={() => {
                    setOpen(false);
                }}
            >
                <div>
                    <img class="modal-img" src={require(`${src}`)} alt="img" />
                    <div class="project-text-wrapper">
                        <p class="project-text">Stack: {stack}</p>
                        <p class="project-text">{text}</p>
                        <p class="project-text"> {text2 && text2}</p>
                        <a class="project-text" href={link} target="_blank">  <button className="moreInfoButton">Go live --> </button></a>
                    </div>
                </div>
            </Modal>
        </div >
    );
}


