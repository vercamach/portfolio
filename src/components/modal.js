import React from "react";

// import { CancelIcon } from "../icons/icons";

export const Modal = ({
    children,
    isOpen,
    onClose,
    style = {}
}: {
        children: React.Element<*>,
        isOpen: boolean,
        onClose: (e: Event) => void,
        style?: { [string]: any }
    }) => {
    return (
        <div
            role="dialog"
            aria-hidden={!isOpen}
            style={{
                zIndex: 101,
                top: 0,
                display: "flex",
                flexDirection: "column",
                transition: "all 200ms",
                opacity: isOpen ? 1 : 0,
                visibility: isOpen ? "visible" : "hidden"
            }}
        >
            <div
                onClick={onClose}
                style={{
                    position: "fixed",
                    top: 0,
                    left: 0,
                    width: "100%",
                    height: "100%",
                    backgroundColor: "#10234D",
                    opacity: 0.3,
                    zIndex: 101
                }}
            />
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    position: "fixed",
                    top: "50%",
                    left: "50%",
                    transform: "translate(-50%, -50%)",
                    transition: "500ms opacity",
                    zIndex: 1000,
                    fontFamily: "monospace"
                }}
            >
                <div
                    style={{
                        display: "flex",
                        minHeight: 50,
                        padding: 10
                    }}
                >
                    <div
                        style={{
                            background: "white",
                            borderRadius: 5,
                            boxShadow: "3px 3px 3px #878787",
                            width: "100vw",
                            maxWidth: 800
                        }}
                    >
                        <div
                            data-testid="close"
                            onClick={onClose}
                            style={{
                                position: "absolute",
                                right: 15,
                                top: 15,
                                cursor: "pointer",
                                zIndex: 1000,
                                width: 80,
                                height: 80
                            }}
                        >
                            {/* <CancelIcon /> */}
                        </div>
                        {children}
                    </div>
                </div>
            </div>
        </div>
    );
};
