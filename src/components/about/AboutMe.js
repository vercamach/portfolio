import React from "react";
import "./about-me.css"

export default function AboutMe() {
    return (

        <div className="about-me-wrap">

            <div className="img-wrap">
                <img className="profile-img" src={require("./b-w.jpg")} alt=" me" />
            </div>

            <div className="text-wrap">
                <h1>About me</h1>
                <h3>Hi, I am Veronika Machackova, React developer from Czechia.</h3>
                <h3>  I am working mostly with ReactJS, Gatsby, Git, HTML, CSS and JavaScript. I have also experiwnce with PHP, relational databases and learning experience with Python and Django.</h3>
                <h3>I am a natural problem solver, eager to learn new technologies and to find solutions to complex problems.</h3></div>
        </div>
    )
}